angular.module('conFusion.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$localStorage,$rootScope,$ionicPlatform,$cordovaCamera,$cordovaImagePicker,AuthFactory,$cordovaToast) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
      $scope.loginData = $localStorage.getObject('userinfo','{}');
     $scope.loggedIn = false;
 if(AuthFactory.isAuthenticated()) {
        $scope.loggedIn = true;
        $scope.username = AuthFactory.getUsername();
    }
    
    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function () {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function () {
        $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function () {
        console.log('Doing login', $scope.loginData);
        $localStorage.storeObject('userinfo',$scope.loginData);

        AuthFactory.login($scope.loginData);

        $scope.closeLogin();
    };
    
    $scope.logOut = function() {
       AuthFactory.logout();
        $scope.loggedIn = false;
        $scope.username = '';
    };
      
    $rootScope.$on('login:Successful', function () {
        $scope.loggedIn = AuthFactory.isAuthenticated();
         $ionicPlatform.ready(function () { 
        $cordovaToast
                  .show('login:Successful', 'long', 'center')
                  .then(function (success) {
                      // success
                  }, function (error) {
                      // error
                  });
         });
        $scope.username = AuthFactory.getUsername();
    });
    
$scope.reservation = {};

  // Create the reserve modal that we will use later
  $ionicModal.fromTemplateUrl('templates/reserve.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.reserveform = modal;
  });

  // Triggered in the reserve modal to close it
  $scope.closeReserve = function() {
    $scope.reserveform.hide();
  };

  // Open the reserve modal
  $scope.reserve = function() {
    $scope.reserveform.show();
  };

  // Perform the reserve action when the user submits the reserve form
  $scope.doReserve = function() {
    console.log('Doing reservation', $scope.reservation);

    // Simulate a reservation delay. Remove this and replace with your reservation
    // code if using a server system
    $timeout(function() {
      $scope.closeReserve();
    }, 1000);
  };
  
 $scope.registration = {};
     // Create the registration modal that we will use later
   $ionicModal.fromTemplateUrl('templates/register.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.registerform = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeRegister = function () {
        $scope.registerform.hide();
    };

    // Open the login modal
    $scope.register = function () {
        $scope.registerform.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doRegister = function () {
        console.log('Doing registration', $scope.registration);
        $scope.loginData.username = $scope.registration.username;
        $scope.loginData.password = $scope.registration.password;

        AuthFactory.register($scope.registration);
            $scope.closeRegister();
    };
       
    $rootScope.$on('registration:Successful', function () {
        $localStorage.storeObject('userinfo',$scope.loginData);
        $ionicPlatform.ready(function () { 
        $cordovaToast
                  .show('Regitered Successful', 'long', 'center')
                  .then(function (success) {
                      // success
                  }, function (error) {
                      // error
                  });
         });
    });
    $ionicPlatform.ready(function() {
        var options = {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 100,
            targetHeight: 100,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };
         $scope.takePicture = function() {
            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.registration.imgSrc = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                console.log(err);
            });

            $scope.registerform.show();

        };
                
            // Image picker will load images according to these settings
            var options2 = {
                destinationType:Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY
            };
 
     $scope.pickImage = function() { $cordovaCamera.getPicture(options2).then(function (imageURI) {
                // Loop through acquired images
      $scope.registration.imgSrc= "data:image/jpeg;base64," + imageURI;
            }, function(error) {
                console.log('Error: ' + JSON.stringify(error));    // In case of error
            });
              $scope.registerform.show();
         };
        });
         
    
   
})

  .controller('MenuController', ['$scope','menuFactory','cartFactory','favoriteFactory','baseURL', '$ionicListDelegate','$ionicPlatform', '$cordovaLocalNotification', '$cordovaToast', function($scope,menuFactory,cartFactory,favoriteFactory,baseURL,$ionicListDelegate,$ionicPlatform, $cordovaLocalNotification, $cordovaToast) {
         $scope.baseURL= baseURL;   
            $scope.tab = 1;
            $scope.filtText = '';
            $scope.showDetails = false;
            $scope.showMenu = false;
            $scope.message = "Loading ...";
            $scope.baseURL=baseURL;
            menuFactory.query(
        function (response) {
            $scope.dishes = response;
            $scope.showMenu = true;

        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        });
            $scope.select2=function(category){
        $scope.filtText=category;
    };
 $scope.clear=function(){
        $scope.filtText="";
    };

    $scope.isSelected = function (checkTab) {
        return ($scope.filtText=== checkTab);
    };
    
            $scope.toggleDetails = function() {
                $scope.showDetails = !$scope.showDetails;
                
                
            };
      $scope.addToFavorites = function(dishid) {
        console.log('Add to favorites', dishid);
        var favorites=favoriteFactory.get();
        favorites.$save({id:dishid});
        $scope.showFavorites = !$scope.showFavorites;
    
        $ionicListDelegate.closeOptionButtons();
          
            $ionicPlatform.ready(function () {
                $cordovaLocalNotification.schedule({
                    id: 1,
                    title: "Added to Favorite"
                }).then(function () {
                    console.log('Added to Favorite ');
                },
                function () {
                    console.log('Failed to add Notification ');
                });

                $cordovaToast
                  .show('Added to Favorite ', 'long', 'center')
                  .then(function (success) {
                      // success
                  }, function (error) {
                      // error
                  });
        });
    }
       $scope.addToCart = function(dishid) {
        console.log('Add to Cart', dishid);
        var carts=cartFactory.get();
        carts.$save({id:dishid});
        $scope.showCart = !$scope.showCart;
    
        $ionicListDelegate.closeOptionButtons();
          
            $ionicPlatform.ready(function () {
                $cordovaLocalNotification.schedule({
                    id: 1,
                    title: "Added to Cart" 
                }).then(function () {
                    console.log('Added to Cart ');
                },
                function () {
                    console.log('Failed to add Notification ');
                });

                $cordovaToast
                  .show('Added to Cart ', 'long', 'center')
                  .then(function (success) {
                      // success
                  }, function (error) {
                      // error
                  });
        });
    }
        }])

        .controller('ContactController', ['$scope', function($scope) {

            $scope.feedback = {mychannel:"", firstName:"", lastName:"", agree:false, email:"" };
            
            var channels = [{value:"tel", label:"Tel."}, {value:"Email",label:"Email"}];
            
            $scope.channels = channels;
            $scope.invalidChannelSelection = false;
                        
        }])

        .controller('FeedbackController', ['$scope', 'feedbackFactory', function($scope,feedbackFactory) {
            
            $scope.sendFeedback = function() {
                
                console.log($scope.feedback);
                
                if ($scope.feedback.agree && ($scope.feedback.mychannel == "")) {
                    $scope.invalidChannelSelection = true;
                    console.log('incorrect');
                }
                else {
                    $scope.invalidChannelSelection = false;
                    feedbackFactory.save($scope.feedback);
                    $scope.feedback = {mychannel:"", firstName:"", lastName:"", agree:false, email:"" };
                    $scope.feedback.mychannel="";
                    $scope.feedbackForm.$setPristine();
                    console.log($scope.feedback);
                }
            };
        }])

        .controller('DishDetailController', ['$scope', '$stateParams','dish', 'menuFactory','favoriteFactory','baseURL','$ionicPopover','$ionicModal','$ionicPlatform', '$cordovaLocalNotification', '$cordovaToast', function($scope, $stateParams,dish, menuFactory,favoriteFactory,baseURL,$ionicPopover,$ionicModal,$ionicPlatform, $cordovaLocalNotification, $cordovaToast) {
            $scope.baseURL=baseURL;
           // $scope.dish = {};
            $scope.showDish = false;
            $scope.message="Loading ...";
            
            $scope.dish = dish;
                 $ionicPopover.fromTemplateUrl('templates/dish-detail-popover.html', {
                    scope: $scope
                  }).then(function(popover) {
                    $scope.popover = popover;
                  });
          $scope.addToFavorites = function(dishid) {
        console.log('Add to favorites', dishid);
        var favorites=favoriteFactory.get();
        favorites.$save({id:dishid});
        $scope.showFavorites = !$scope.showFavorites;
    location.reload();
             
             
               $ionicPlatform.ready(function () {
                $cordovaLocalNotification.schedule({
                    id: 0,
                    title: "Added Favorite",
                    text: $scope.dish.name
                }).then(function () {
                    console.log('Added Favorite '+ $scope.dish.name);
                },
                function () {
                    console.log('Failed to add Notification ');
                });

                $cordovaToast
                  .showLongBottom('Added Favorite '+$scope.dish.name)
                  .then(function (success) {
                      // success
                  }, function (error) {
                      // error
                  });
        });
                $scope.popover.hide();
             
             
            }
        
       $scope.mycomment = {rating:5, comment:"", author:"", date:""};
              $ionicModal.fromTemplateUrl('templates/dish-comment.html', {
            scope: $scope
          }).then(function(modal) {
            $scope.commentform = modal;
          });

          // Triggered in the reserve modal to close it
          $scope.closeComment = function() {
            $scope.commentform.hide();
          };

          // Open the reserve modal
        
          $scope.comment = function() {
            $scope.commentform.show();
          };

          $scope.doComment = function() {
            console.log('submitting comment', $scope.mycomment);
              
              $scope.mycomment.date = new Date().toISOString();
                console.log($scope.mycomment);
                
                $scope.dish.comments.push($scope.mycomment);
        menuFactory.update({id:$scope.dish.id},$scope.dish);
              
              $scope.commentform.hide();
              $scope.popover.hide();
          };    

         
        }])

        .controller('DishCommentController', ['$scope', 'menuFactory', function($scope,menuFactory) {
            
            $scope.mycomment = {rating:5, comment:"", author:"", date:""};
            
            $scope.submitComment = function () {
                
                $scope.mycomment.date = new Date().toISOString();
                console.log($scope.mycomment);
                
                $scope.dish.comments.push($scope.mycomment);
        menuFactory.update({id:$scope.dish.id},$scope.dish);
                
                $scope.commentForm.$setPristine();
                
                $scope.mycomment = {rating:5, comment:"", author:"", date:""};
            }
        }])

        // implement the IndexController and About Controller here

        .controller('IndexController', ['$scope','dish','promo','leader','baseURL', function($scope, dish,promo,leader,baseURL) {
                        $scope.baseURL= baseURL;                
                        $scope.leader = leader;
                        $scope.showDish = false;
                        $scope.message="Loading ...";
                        $scope.dish = dish;
            
                        $scope.promotion = promo;
            
                    }])

        .controller('AboutController', ['$scope', 'leaders','baseURL', function($scope, leaders,baseURL) {
                    $scope.baseURL=baseURL;
                    $scope.leaders = leaders;
                    console.log($scope.leaders);
            
        }])

.controller('FavoritesController', ['$scope','$state', 'favoriteFactory', 'baseURL', '$ionicListDelegate','$ionicPopup','$ionicLoading', '$timeout','$localStorage','$ionicPlatform','$cordovaVibration', function ($scope, $state,favoriteFactory, baseURL, $ionicListDelegate,$ionicPopup,$ionicLoading, $timeout,$localStorage,$ionicPlatform,$cordovaVibration) {

    $scope.baseURL = baseURL;
    $scope.shouldShowDelete = false;
   
    $scope.dishes= {};
     favoriteFactory.query(
        function (response) {
            $scope.dishes = response.dishes;
            $scope.showMenu = true;
        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        });

    console.log($scope.dishes, $scope.favorites);

    $scope.toggleDelete = function () {
        $scope.shouldShowDelete = !$scope.shouldShowDelete;
        console.log($scope.shouldShowDelete);
    }
  
      
    $scope.deleteFavorite = function (index) {
            console.log('Delete favorites', index);
          var confirmPopup = $ionicPopup.confirm({
            title: 'Confirm Delete',
            template: 'Are you sure you want to delete this item?'
        });
       confirmPopup.then(function (res) {
            if (res) {
                console.log('Ok to delete');
                favoriteFactory.delete({id:index});
      $state.go($state.current, {}, {reload: true});
    
                
               $ionicPlatform.ready(function(){
                $cordovaVibration.vibrate(100);
                   
            });
                 
            } else {
                console.log('Canceled delete');
            }
        }); 
        $scope.shouldShowDelete = false;

    }}])
.controller('CartController',['$scope','$state', 'cartFactory', 'baseURL', '$ionicListDelegate','$ionicPopup','$ionicLoading', '$timeout','$localStorage','$ionicPlatform','$cordovaVibration', function ($scope, $state,cartFactory, baseURL, $ionicListDelegate,$ionicPopup,$ionicLoading, $timeout,$localStorage,$ionicPlatform,$cordovaVibration) {

   
    $scope.filtText = '';
    $scope.showDetails = false;
   $scope.shouldShowDelete = false;
    $scope.showMenu = false;
    $scope.message = "Loading ...";
    $scope.sum=0;

    cartFactory.query(
        function (response) {
            $scope.dishes = response.dishes;
            $scope.showMenu = true;

        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        });

  $scope.toggleDelete = function () {
        $scope.shouldShowDelete = !$scope.shouldShowDelete;
        console.log($scope.shouldShowDelete);
    }

     $scope.total = function() {
        var total = 0;
        angular.forEach($scope.dishes, function(dish) {
            total += dish.price ;
        })
     return total
     };
    
    $scope.deleteCart = function(dishid) {
        console.log('Delete item', dishid);
        
        var confirmPopup = $ionicPopup.confirm({
            title: 'Confirm Delete',
            template: 'Are you sure you want to delete this item?'
        });
       confirmPopup.then(function (res) {
            if (res) {
                console.log('Ok to delete');
                cartFactory.delete({id: dishid});
                  $timeout(function() {
      $state.go($state.current, {}, {reload: true});
    }, 500);  
               

                $ionicPlatform.ready(function(){
                $cordovaVibration.vibrate(100);
                });
                 
            } else {
                console.log('Canceled delete');
            }
            });
    };
    $scope.checkout=function(){
        console.log('Checkout Successfully');
       
        cartFactory.delete();
         $timeout(function() {
      $state.go($state.current, {}, {reload: true});
    }, 500);  
        $ionicPlatform.ready(function(){
        $cordovaVibration.vibrate(100);        
        });
       
    }
    $scope.Change=function(cash){
         var Change=0;
        Change=  cash*100-$scope.total();
         return Change;
    }
    $scope.shouldShowDelete = false;
}])

;

